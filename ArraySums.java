import java.util.Arrays;
import java.util.Scanner;

public class ArraySums {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int m = scanner.nextInt();
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println(Arrays.toString(findSums(array, k, m)));
    }

    public static int[] findSums(int[] array, int start, int end) {
        int[] sums = new int[end - start - 1];
        for (int i = start,j=0; i <= end - 2; i++,j++) {
                sums[j] = array[i] + array[i + 1] + array[i + 2];
        }
        return sums;
    }
}
