import java.util.Scanner;

public class Coprime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        coprime(a,b,c);
    }

    public static int gcd(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    public static void coprime(int a, int b, int c) {
        if (gcd(gcd(a, b), c) == 1) {
            System.out.println("Given three numbers are coprime");
        } else {
            System.out.println("Given three numbers are not coprime");
        }
    }
}
