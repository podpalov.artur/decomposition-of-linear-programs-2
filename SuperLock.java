import java.util.Arrays;
import java.util.Random;

public class SuperLock {
    public static void main(String[] args) {
        Random rand = new Random();
        int n1 = 0;
        int n2 = 0;
        while ((n1 + n2) < 4 || n1 == 0 || n2 == 0 || (n1+n2)>9) {
            n1 = rand.nextInt(6);
            n2 = rand.nextInt(6);
        }
        System.out.println(Arrays.toString(password(n1, n2)));
    }

    public static int[] password(int n1, int n2) {
        int[] pass = new int[10];
        pass[0] = n1;
        pass[1] = n2;
        for (int i = 2; i <= 9; i++) {
            pass[i] = 10 - (pass[i - 2] + pass[i - 1]);
        }
        return pass;
    }
}
