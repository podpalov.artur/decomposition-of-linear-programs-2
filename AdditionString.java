public class AdditionString {
    public static void main(String[] args) {
        String n1 = "31296";
        String n2 = "512411";
        System.out.println(stringAdder(n1, n2));
    }

    public static String stringAdder(String n1, String n2) {
        StringBuilder result = new StringBuilder();
        int n1digit = n1.length() - 1;
        int n2digit = n2.length() - 1;
        int carry = 0;
        while (n1digit >= 0 || n2digit >= 0) {
            int digit1;
            int digit2;
            if (n1digit >= 0) {
                digit1 = n1.charAt(n1digit) - '0';
            } else {
                digit1 = 0;
            }
            if (n2digit >= 0) {
                digit2 = n2.charAt(n2digit) - '0';
            } else {
                digit2 = 0;
            }
            int sum = digit1 + digit2 + carry;
            carry = sum / 10;
            result.append(sum % 10);
            n1digit--;
            n2digit--;
        }

        if (carry > 0) {
            result.append(carry);
        }
        return result.reverse().toString();
    }
}