import java.util.ArrayList;
import java.util.Scanner;

public class Armstrong {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        System.out.println(checkArmstrong(k));
        System.out.println(allArmstrong(k));
    }

    public static boolean checkArmstrong(int a) {
        int n = Integer.toString(a).length();
        int sum = 0;
        int temp = a;
        while (temp > 0) {
            int digit = temp % 10;
            sum += Math.pow(digit, n);
            temp = temp / 10;
        }
        return sum == a;
    }

    public static ArrayList allArmstrong(int k) {
        ArrayList<Integer> armstrongs = new ArrayList<>();
        for (int i = 1; i < k; i++) {
            if (checkArmstrong(i)) {
                armstrongs.add(i);
            }
        }
        return armstrongs;
    }
}
